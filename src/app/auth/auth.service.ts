import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { User } from './user.model';
import { map,tap } from 'rxjs/operators'
export interface AuthResponseData{
  kind:string;
  idToken:string;
  email:string;
  refreshToken:string;
  loaclId:string;
  expiesIn:string;
  registered?:boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
private _user= new BehaviorSubject<User>(null);

  constructor(private http: HttpClient) { }
  get userAuthenticated(){
    return this._user.asObservable().pipe(
      map(user=>{
        if(user){
          return !!user.token
        }else{
          return false;
        }
      }
        ));
  }
  login(email,password){
   return this.http.post<AuthResponseData>(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.fireBaseAPIKey}`,
    {email:email,password:password}).pipe(tap(userData=>{
      this.setUserData.bind(this);
    }))
   
  }
  get userId(){
    return this._user.asObservable().pipe(map(user=>
      {
        if(user){
         return user.id;
        }else{
          return null;
        }
      }))
  }
  logout(){
    this._user.next(null);
  }
  signup(email:string,password:string){
    return this.http.post<AuthResponseData>(`https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${environment.fireBaseAPIKey}`
    ,{email:email,password:password,returnSecureToken:true}).pipe(tap(userData=>{
      this.setUserData.bind(this);
    }))
  };
  private setUserData(userData:AuthResponseData){
    const expirationTime =new Date(new Date().getTime()+ (+userData.expiesIn*1000));
      this._user.next(new User(userData.loaclId,userData.email,userData.idToken,expirationTime));
  }
  
}
