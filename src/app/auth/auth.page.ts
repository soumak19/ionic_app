import { Component, OnInit } from '@angular/core';
import { AuthService,AuthResponseData } from './auth.service';
import { Router } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { TimeoutError, Observable } from 'rxjs';
import { NgForm } from '@angular/forms';



@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
  private isLoding = false;
  private isLogin=true;
  constructor(private authService:AuthService,
    private route:Router,
    private loadingctrl:LoadingController,
    private alertCtrl:AlertController) { }

  ngOnInit() {
  }
  Authenticate(email,password){
    
    this.loadingctrl.create({keyboardClose:true,message:'Logging in..'}).then(loadingEL=>{
      loadingEL.present();
      let authObs :Observable<AuthResponseData>;
      if(this.isLogin){
        authObs= this.authService.login(email,password);
      }else{
        authObs=this.authService.signup(email,password);
      }
      authObs.subscribe(resdata=>{
        console.log(resdata);
        this.isLoding=false;
        loadingEL.dismiss();
        this.route.navigateByUrl('places/tabs/discover');
       },errRes=>{
         loadingEL.dismiss();
         const code=errRes.error.error.message;
         let message='Could not sign you up,please try again.';
         if(code === 'EMAIL_EXISTS'){
           message='this email address already exixts!';
         }else if(code === 'EMAIL_NOT_FOUND'){
           message ='E-mail address could not found.';
         }else if(code === 'INVALID_PASSWORD'){
           message ='This password is not correct.';
         }
         this.showAlert(message);
       })
    })
    this.isLoding=true;
  
    
  }
  onSubmit(form:NgForm){
    if(!form.valid){
      return;
    }
    const email=form.value.email;
    const password =form.value.password;
    
      this.Authenticate(email,password);
    
  }
  onSwitchAuthMode(){
    this.isLogin=!this.isLogin;
  }
  private  showAlert(message:string){
    this.alertCtrl.create({
      header:'Authentication failed',
      message:message,
      buttons:['okey']
    }).then(alertEl=>alertEl.present());
  }
}
