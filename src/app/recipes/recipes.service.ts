import { Injectable } from '@angular/core';
import {Recipe} from './recipe.model'

@Injectable({
  providedIn: 'root'
})
export class RecipesService {
 private recipes :Recipe[]=[
    {
      id:'r1',
    title:'Burgger',
    imageUrl:'../../assets/Image/pexels-photo-376464.jpeg',
    ingredients:['Franh frie', 'pork meat','salad']
   },
   {
     id:'r2',
   title:'Schnitzel',
   imageUrl:'../../assets/Image/ctyp-nordic-state-fair-new.jpg',
   ingredients:['Spagatiti', 'meat','Tomatoes']
  }
  ]
  constructor() { }
  getAllRecipes(){
    return [...this.recipes];
  }
  getRecipe(recipeId:String){
    return {
      ...this.recipes.find(recipe=>{
      return recipe.id === recipeId;
    })
   
  };
  }
  deleteRecipe(recipeId:string){
    this.recipes = this.recipes.filter(recipe=>{
      return recipe.id !== recipeId;
    })
    console.log(this.recipes);
  }
}
