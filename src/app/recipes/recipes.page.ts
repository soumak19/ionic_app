import { Component, OnInit ,OnDestroy} from '@angular/core';
import{Recipe} from './recipe.model'
import { RecipesService } from './recipes.service';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.page.html',
  styleUrls: ['./recipes.page.scss'],
})
export class RecipesPage implements OnInit,OnDestroy {
  recipes:Recipe[];
  constructor(private recipeServise: RecipesService) { }

  ngOnInit() {
 
   console.log(this.recipes);
  }
   ionViewWillEnter(){
    this.recipes= this.recipeServise.getAllRecipes();
     console.log('ionViewWillEnter');
   }
   ionViewDidEnter(){
     console.log('ionViewDidEnter');
   }
   ionViewWillLeave(){
     console.log('ionViewWillLeave');
   }
   ionViewDidLeave(){
    console.log('ionViewDidLeave');
  }
  ngOnDestroy(){
    console.log('OnDestroy');
  }

}
