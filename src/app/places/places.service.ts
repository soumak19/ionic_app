import { Injectable } from '@angular/core';
import {Place} from './place.model';
import { AuthService } from '../auth/auth.service';
import { BehaviorSubject, of } from 'rxjs';
import { take,map,tap,delay,switchMap} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
interface PlaceData{
  availableFrom:string,
  availableTo:string,
  description:string,
  imageUrl:string,
  price:number,
  title:string,
  userId:string
}

@Injectable({
  providedIn: 'root'
})
export class PlacesService {

private _place = new BehaviorSubject<Place[]>(
  [
    // new Place(
    //   'p1',
    //   'Kolkata',
    //   'The city of joy!!',
    //   '../../assets/Image/1883939.jpg',
    //   144.99 ,
    //   new Date('2020-01-01') ,
    //   new Date('2020-12-31'),
    //   "xyz"
  
    // ),
    // new Place(
    //   'p2',
    //   'Goa',
    //   'The romantic place in India!!',
    //   '../../assets/Image/Goa-HD-Sunset-Wallpapers.jpg',
    //   123.98,
    //   new Date('2020-01-01') ,
    //   new Date('2020-12-31'),
    //   "soumak"
      
    // ), 
    // new Place(
    //   'p3',
    //   'Delhi',
    //   'The heart of India!',
    //   '../../assets/Image/wp1891562.jpg',
    //   150.98,
    //   new Date('2020-01-01') ,
    //   new Date('2020-12-31'),
    //   "soumak"
    // )
  ]
);

get places(){
  return this._place.asObservable();
}
fatchPlaces(){
  return this.http.get<{[key:string]: PlaceData}>('https://ionic-angular-prac.firebaseio.com/offerd-places.json')
  .pipe(map(resData=>{
const places=[];
    for(const key in resData){
      if(resData.hasOwnProperty(key)){
        places.push(new Place(key,
          resData[key].title,
          resData[key].description,
          resData[key].imageUrl,
          resData[key].price,
          new Date(resData[key].availableFrom),
           new Date(resData[key].availableTo),
           resData[key].userId));
      }
    }
    return places;
  }),
  tap(places=>{
    this._place.next(places);
  }))
}
getPlace(placeId:string){

    return this.http.get<PlaceData>(`https://ionic-angular-prac.firebaseio.com/offerd-places/${placeId}.json`).pipe(
      map(placeData=>{
        return new Place(placeId,placeData.title,placeData.description,placeData.imageUrl,placeData.price,new Date(placeData.availableFrom),new Date(placeData.availableTo),placeData.userId)
      })
      )
    
    
  }
  uploadImage(image: File) {
    const uploadData = new FormData();
    uploadData.append('image', image);

    return this.http.post<{imageUrl: string, imagePath: string}>(
      'https://us-central1-ionic-angular-prac.cloudfunctions.net/storeImage',
      uploadData
    );
  }
  constructor(private authService:AuthService,private http:HttpClient) { }
  addPlace(title:string,description:string,price:number,dateFrom:Date,dateTo:Date,imageUrl: string){
    let genetaredId:string;
    let newPlace:Place;
    this.authService.userId.pipe(take(1),
      switchMap(userId=>{
        if(!userId){
          throw new Error('User does not exist!')
        }
         newPlace= new Place(
          Math.random().toString(),
          title,
          description,
          imageUrl,
          price,
          dateFrom,
          dateTo,
         userId
    
      );
      return this.http.post<{name:string}>('https://ionic-angular-prac.firebaseio.com/offerd-places.json',{...newPlace,id:null});
      }),
      switchMap(resData=>
    {
    genetaredId = resData.name;
    return this.places;
    }),
    take(1),
    tap(places=>{
   newPlace.id = genetaredId;
   this._place.next(places.concat(newPlace));
  }));
  // return this.places.pipe(take(1),delay(1000),tap(places=>{
  //   this._place.next(places.concat(newPlace));
  // }))
 
  }
  updatePlace(placeId:string,title:string,description:string){
    let updatedPlaces:Place[];
   return this.places.pipe(take(1),switchMap(places=>{
     if(!places || places.length <=0){
       return this.fatchPlaces();
     }else{
       return of(places);
     }
      
    }),
    switchMap(places=>{
      const updatedPlaceIndex = places.findIndex(pl=>pl.id === placeId);
       updatedPlaces = [...places];
      const oldPlace= updatedPlaces[updatedPlaceIndex];
      updatedPlaces[updatedPlaceIndex] = new Place(oldPlace.id,title,description,oldPlace.imageUrl,oldPlace.price,oldPlace.availableFrom,oldPlace.availableTo,oldPlace.userId);
      return this.http.put(`https://ionic-angular-prac.firebaseio.com/offerd-places/${placeId}.json`,
       {... updatedPlaces[updatedPlaceIndex] ,id:null}
      )
    })
    ,tap(()=>{
      this._place.next(updatedPlaces);
    }))
    
   
    
  }
}
