import { Component, OnInit, OnDestroy } from '@angular/core';
import { PlacesService } from '../places.service';
import {Place} from '../place.model'
import { ActivatedRoute, Router } from '@angular/router';
import { IonItemSliding } from '@ionic/angular';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-offers',
  templateUrl: './offers.page.html',
  styleUrls: ['./offers.page.scss'],
})
export class OffersPage implements OnInit,OnDestroy {
  offers:Place[];
  placesSub:Subscription;
  isLoading = false;
  constructor(private placeService:PlacesService,private router:Router ) { }

  ngOnInit() {
  this.placesSub=this.placeService.places.subscribe(places=>{
   this.offers = places;
 })
  }
  ionViewWillEnter(){
    this.isLoading = true;
    this.placeService.fatchPlaces().subscribe(()=>{
      this.isLoading = false;
    }
    );
     
  }
  onEdit(offerId:string,slideItem:IonItemSliding){
    slideItem.close();
    this.router.navigate(['/','places','tabs','offers','edit', offerId]);
   
  }
  ngOnDestroy(){
    if(this.placesSub){
      this.placesSub.unsubscribe();
    }
  }
}
