import { Component, OnInit, OnDestroy } from '@angular/core';
import { Place } from '../place.model';
import { PlacesService } from '../places.service';
import { MenuController } from '@ionic/angular';
import { SegmentChangeEventDetail } from '@ionic/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import{take} from 'rxjs/operators'

@Component({
  selector: 'app-discover',
  templateUrl: './discover.page.html',
  styleUrls: ['./discover.page.scss'],
})
export class DiscoverPage implements OnInit,OnDestroy {
 loadedPlaces:Place[];
 listedlodedPlaces:Place[];
 private placesSub:Subscription;
 relevantPlaces:Place[];
 isLoading=false;
  constructor(private placesService:PlacesService,private menuCtrl:MenuController,private authService:AuthService) { }

  ngOnInit() {
    this.placesSub= this.placesService.places.subscribe(places=>{
    this.loadedPlaces = places;
    this.relevantPlaces = this.loadedPlaces;
    this.listedlodedPlaces=this.relevantPlaces.slice(1);
  })
   
  }
  ionViewWillEnter(){
   this.  isLoading = true;
    this.placesService.fatchPlaces().subscribe(()=>{
      this.isLoading= false;
    });
  }
  // onOpenMenu(){
  //  this.menuCtrl.toggle();
  // }
  onFilterUpdate(event:CustomEvent<SegmentChangeEventDetail>){
    this.authService.userId.pipe(take(1)).subscribe(userId=>{
      if(event.detail.value === 'All'){
        this.relevantPlaces = this.loadedPlaces;
        this.listedlodedPlaces=this.relevantPlaces.slice(1);
      }else{
        this.relevantPlaces=this.loadedPlaces.filter(place => userId);
      } ;
      this.listedlodedPlaces=this.relevantPlaces.slice(1);
    })
 

  }
  ngOnDestroy(){
    if(this.placesSub){
     this.placesSub.unsubscribe();
    }
  }
}
