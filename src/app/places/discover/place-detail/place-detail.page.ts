import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, ActionSheetController, ModalController, LoadingController, AlertController } from '@ionic/angular';
import { CreateBookingComponent } from '../../../bookings/create-booking/create-booking.component';
import { Place } from '../../place.model';
import { PlacesService } from '../../places.service';
import { Subscription } from 'rxjs';
import { BookingsService } from '../../../bookings/bookings.service';
import { AuthService } from '../../../auth/auth.service';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-place-detail',
  templateUrl: './place-detail.page.html',
  styleUrls: ['./place-detail.page.scss'],
})
export class PlaceDetailPage implements OnInit,OnDestroy{
  private place:Place;
  private placeSub:Subscription;
  isBookable = false;
  isLoading = false;
  constructor(
    private router:Router,
    private route:ActivatedRoute,
    private nevCtrl:NavController,
    private actionSheetCtrl:ActionSheetController,
    private modalCtrl:ModalController,
    private placeService:PlacesService,
    private bookingService:BookingsService,
    private loadingCtrl:LoadingController,
    private authService:AuthService,
    private alertCtrl:AlertController,

    ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap=>{
      if(!paramMap.has('placeId')){
        this.nevCtrl.navigateBack('places/tabs/discover');
        return;
      }
     this.isLoading =true;
     let fetchUserId: string;
     this.authService.userId.pipe(
      switchMap(userId=>{
      if(!userId){
        throw new Error('Found no user!');
      }
      fetchUserId=userId;
      return  this.placeService.getPlace(paramMap.get('placeId'))
     }))
     .subscribe(
       place=>{
        this.place= place;
        this.isBookable = place.userId !== fetchUserId;
        this.isLoading =false;
        console.log(place,this.isLoading);
      },error=>{
        console.log(error);
        this.alertCtrl.create({
          header:'An error ocurred!',
          message:'could not load place..',
          buttons:[{text:'Okey',handler:()=>{
            this.router.navigate(['/places/tabs/discover'])
          }}]
        }).then(alertEl=>{
          alertEl.present();
        })
      })
    })
  }
  onBookPlace(){
    this.actionSheetCtrl.create({
      header:'Choose an Action',
      buttons:[
        {
        text:'select Date',
        handler:()=>{
          this.openBookingModal('select');
        }
      },{
        text:'Random Date',
        handler:()=>{
          this.openBookingModal('random');
        }
      },
      {
        text:'cancel',
        role:'cancel'
      }
    ]
    }).then(ActionEl=>{
      ActionEl.present();
    })
   
  // this.nevCtrl.navigateBack('/places/tabs/discover');
 
  }
  openBookingModal(mode:'select'|'random'){
    this.modalCtrl.create({
      component:CreateBookingComponent,
      componentProps:{selectedPlace:this.place,selectedMode:mode}
    }).then(modaEl=>{
      modaEl.present();
      return modaEl.onDidDismiss();
    }).then(resultData=>{
      if(resultData.role === 'confirm'){
        this.loadingCtrl.create({
          message:'Booking place...'
        }).then(loadingEl=>{
          loadingEl.present();
          const data= resultData.data.bookingData;
        this.bookingService.addBooking(this.place.id,
          this.place.title,
          this.place.imageUrl,
          data.firstName,
          data.lastName,
          data.guestNumber,
          data.startDate,
          data.endDate
          ).subscribe(()=>{
            loadingEl.dismiss();
          })
        })
        
      }
    })
  }
  ngOnDestroy(){
    if(this.placeSub){
      this.placeSub.unsubscribe();
    }
  }
}
