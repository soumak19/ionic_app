import { Component, OnInit, OnDestroy } from '@angular/core';
import { Booking } from './booking.model';
import { BookingsService } from './bookings.service';
import { Subscription } from 'rxjs';
import { IonItemSliding, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.page.html',
  styleUrls: ['./bookings.page.scss'],
})
export class BookingsPage implements OnInit,OnDestroy{
loadedBookings:Booking[];
bookingSub:Subscription;
isLoading=true;
  constructor(private bookingService:BookingsService,private loadingCtrl:LoadingController) { }

  ngOnInit() {
    this.bookingService.bookings.subscribe(bookings=>{
      this.loadedBookings = bookings;
      console.log(this.loadedBookings);
    })
  }
  onCancelBooking(bookingId:string,slidingEl:IonItemSliding){
    slidingEl.close();
    this.loadingCtrl.create({message:'cancelling..'}).then(loadingEl=>{
      this.bookingService.cancelBooking(bookingId).subscribe(
        ()=>{
          loadingEl.dismiss();
        }
      );
    })

  
  }
  ionViewWillEnter(){
    this.isLoading=true;
    this.bookingService.fetchBookings().subscribe();
    this.isLoading = false;
    console.log(this.loadedBookings);
  }
  ngOnDestroy(){
    if(this.bookingSub)
    this.bookingSub.unsubscribe();
  }

}
